package com.abhiandroid.viewpagerexample.myapplication;

/**
 * Created by abhiandroid on 9/10/17.
 */

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.app.ProgressDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AppleFragment extends Fragment {
    private ProgressDialog pd;
    ArrayList<String> dataTemp=new ArrayList<String>();
    ArrayList<BarDataSet> yAxis;
    ArrayList<BarEntry> yValues;
    ArrayList<String> xAxis1;
    BarEntry values ;
    BarChart chart;
    BarData data;
    TextView tvdataTemp1,tvdataTemp2;
    public AppleFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_apple, container, false);
        //declaretv
        tvdataTemp1 =(TextView) v.findViewById(R.id.dataSatu) ;
        tvdataTemp2 = (TextView) v.findViewById(R.id.dataDua);
                pd = new ProgressDialog(getContext());
        pd.setMessage("loading");


        // Log.d("array",Arrays.toString(fullData));
        chart = (BarChart) v.findViewById(R.id.chart);
        load_data_from_server();
        return v;
    }


    public void load_data_from_server() {
        pd.show();
        String url = "http://103.82.241.72/faridskrip/";
        xAxis1 = new ArrayList<>();
        yAxis = null;
        yValues = new ArrayList<BarEntry>();


        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("string",response);

                        try {

                            JSONArray jsonarray = new JSONArray(response);

                            for(int i=0; i < jsonarray.length(); i++) {

                                JSONObject jsonobject = jsonarray.getJSONObject(i);


                                String score = jsonobject.getString("y").trim();
                                String name = jsonobject.getString("pos_id").trim();
                                String temperature = jsonobject.getString("temperature").trim();

                                dataTemp.add(temperature);
                                xAxis1.add(name);

                                values = new BarEntry(Float.valueOf(score),i);
                                yValues.add(values);

                            }
                            tvdataTemp1.setText(dataTemp.get(0));
                            tvdataTemp2.setText(dataTemp.get(1));
                        } catch (JSONException e) {
                            e.printStackTrace();


                        }





                        BarDataSet barDataSet1 = new BarDataSet(yValues, "Point RainFall");
                        int[] colors = new int[] {Color.GREEN, Color.YELLOW, Color.GREEN, Color.BLUE, Color.GRAY, Color.BLACK};
                        barDataSet1.setColors(colors);



                        yAxis = new ArrayList<BarDataSet>();
                        yAxis.add(barDataSet1);
                        String names[]= xAxis1.toArray(new String[xAxis1.size()]);
                        data = new BarData(names,yAxis);
                        chart.setData(data);
                        chart.setDescription("");
                        chart.animateXY(2000, 2000);
                        chart.invalidate();
                        pd.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error != null){

                            Toast.makeText(getActivity().getApplicationContext(), "Something went wrong.", Toast.LENGTH_LONG).show();
                            pd.hide();
                        }
                    }
                }

        );

        MySingleton.getInstance(getActivity().getApplicationContext()).addToRequestQueue(stringRequest);

    }

}
