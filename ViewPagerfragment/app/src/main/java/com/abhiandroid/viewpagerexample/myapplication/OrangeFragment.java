package com.abhiandroid.viewpagerexample.myapplication;

/**
 * Created by abhiandroid
 */

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OrangeFragment extends Fragment {
    private ProgressDialog pd1;
    ArrayList<LineDataSet> yAxisO;
    ArrayList<Entry> yValuesO;
    ArrayList<String> xAxis1O;
    ArrayList<Entry> vals1 = new ArrayList<Entry>();
    LineChart lineChart;
    public OrangeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v1 = inflater.inflate(R.layout.fragment_orange, container, false);
        lineChart = (LineChart) v1.findViewById(R.id.linechart1);
        // Inflate the layout for this fragment
        pd1 = new ProgressDialog(getContext());
        pd1.setMessage("loading");
        load_data_from_server1();
        return v1;
    }

    public void load_data_from_server1() {
        pd1.show();
        String urlO = "http://103.82.241.72/faridskrip/soilh";
        xAxis1O = new ArrayList<>();
        yAxisO = null;
        yValuesO = new ArrayList<Entry>();


        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                urlO,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("string", response);

                        try {

                            JSONArray jsonarray = new JSONArray(response);

                            for (int i = 0; i < jsonarray.length(); i++) {

                                JSONObject jsonobject = jsonarray.getJSONObject(i);


                                String score = jsonobject.getString("y").trim();
                                String name = jsonobject.getString("waktu").trim();


                                xAxis1O.add(name);

                                //values = new Entry(Float.valueOf(score),i);
                                yValuesO.add(new Entry(Float.valueOf(score), i));

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();


                        }


                        LineDataSet lineDataSet;
                        lineDataSet = new LineDataSet(yValuesO, "Dataset 1");


                        yAxisO = new ArrayList<>();
                        yAxisO.add(lineDataSet);
                        String names[] = xAxis1O.toArray(new String[xAxis1O.size()]);
                        LineData data = new LineData(names, yAxisO);
                        lineChart.setData(data);
                        lineChart.setDescription("");
                        lineChart.animateXY(2000, 2000);
                        lineChart.invalidate();
                        pd1.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {

                            Toast.makeText(getActivity().getApplicationContext(), "Something went wrong.", Toast.LENGTH_LONG).show();
                            pd1.hide();
                        }
                    }
                }

        );

        MySingleton.getInstance(getActivity().getApplicationContext()).addToRequestQueue(stringRequest);

    }

}
